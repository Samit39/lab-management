<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Users extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        //
        $this->permission_name = "User";
        if(!$this->check_permission($this->permission_name))
        {
            $this->session->set_flashdata('error', 'Sorry You Dont Have Necessary Permission!');
            redirect('dashboard');
        }
        //
        $this->load->model('groups/groups_model', 'groups');
        $this->load->model('users_model', 'users');
    }

    public function index()
    {
        $this->set_log();
        $data = array();
        $data['breadcrumbs_title'] = "User Manager";
        $data['content'] = '_list';

        $data['user_list'] = $this->users->getAllUsers();
        // pr($data);
        // die();
        $this->load->view('common/header');
        $this->load->view('common/body', $data);
    }

    public function create()
    {   
        $data = array();
        $data['is_edit'] = false;
        $data['breadcrumbs_title'] = "Add Users";
        $data['content'] = '_form';
        $data['groups'] = $this->groups->getAllGroupsinuser();
        $hdata['addJs'] = array('assets/js/pages/users.js');
        $id = segment(3);

        
        if ($id == '') {

        } else {
            $data['user'] = $this->users->getUser($id);
            $data['is_edit'] = true;
        }

        if ($_POST) {

            $post = $this->security->xss_clean($this->input->post());
            // $this->form_validation->set_rules('confirm_password','Confirm Password','trim|required|callback_password_check');
            $this->form_validation->set_rules($this->users->rules());
            if ($id == '') {
                $this->form_validation->set_rules("username", "Username", "required|trim", array('is_unique' => 'This username is already taken. Pleasetry different one'));
                $this->form_validation->set_rules("password", "Password", "trim|required|min_length[6]");
                $this->form_validation->set_rules("confirm_password", "Confirm Password", "required|trim|matches[password]");
            }
            if ($this->form_validation->run($this) == false) {

            } else {
                unset($post['confirm_password']);
                if ($id == '') {

                    $valid_token = false;
                    do {
                        // $token = $this->generate_random_key();
                        $token = generate_random_string(5);
                        $valid_token = $this->users->validate_token($token);
                    } while (!$valid_token);
                    if($this->users->check_if_username_already_exists($post['username']))
                    {
                        $this->session->set_flashdata('error', 'Username Already Exists.');
                    }
                    else if($this->users->check_if_email_already_exists($post['email']))
                    {
                        $this->session->set_flashdata('error', 'Email Already Exists.');
                    }
                    else
                    {
                        $post['usercode'] = $token;
                        $post['created'] = date('Y-m-d H:i:s', time());
                        $post['password'] = md5($post['password']);
                        $id = $this->users->save($post);
                        //
                        $this->set_log($id,"User Created with username - ". $post['username'],5);
                        //
                        $this->session->set_flashdata('success', 'Users added to database');
                        //
                        redirect('users');
                    }
                    // $activity = array(
                    //     'action' => 'Add',
                    //     'model' => $this->model_name,
                    //     'model_id' => $id,
                    // );
                } else {
                    $res = $this->users->save($post, array('id' => $id));
                    $this->set_log($id,"User Edited with username - ". $post['username'],6);
                    $this->session->set_flashdata('success', 'Users edited in database');
                    redirect('users');
                    // $activity = array(
                    //     'action' => 'Update',
                    //     'model' => $this->model_name,
                    //     'model_id' => $id,
                    // );
                }
            }
        }

        $this->load->view('common/header', $hdata);
        $this->load->view('common/body', $data);
    }
    public function password_check($str)
    {
        if ($this->input->post('password') == $str) {
            return true;
        } else {
            $this->form_validation->set_message('password', 'This field should be same as password');
            return false;
        }
    }

    public function generate_random_key($length = 5)
    {
        $key = '';
        list($usec, $sec) = explode(' ', microtime());
        mt_srand((float) $sec + ((float) $usec * 100000));

        $inputs = array_merge(range(0, 9), range('A', 'Z'));

        for ($i = 0; $i < $length; $i++) {
            $key .= $inputs{mt_rand(0, 37)};
        }
        return $key;
    }

    public function change_status()
    {
        $user_id = segment(3);
        if($user_id !='')
        {
            $this->users->change_status_of_user($user_id);
            $this->session->set_flashdata('success', 'User Status Successfully Changed.');
        }
        else
        {
            $this->session->set_flashdata('error', 'Parameter error while Changing status.');
        }
        $this->set_log($user_id,"Status Changed Of Userid - ".$user_id,17);
        redirect('users');
    }

    public function delete()
    {
        $user_id = segment(3);
        if($user_id !='')
        {
            $this->users->delete_user($user_id);
            $this->session->set_flashdata('success', 'User Successfully Deleted.');
        }
        else
        {
            $this->session->set_flashdata('error', 'Parameter error while deleting User.');
        }
        $this->set_log($user_id,"Deleted User",7);
        redirect('users');
    }

}
