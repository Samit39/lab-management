<?php
if (!empty($this->input->post())) {
$username = $this->input->post('username');
$address = $this->input->post('address');
$phone = $this->input->post('phone');
$firstname = $this->input->post('firstname');
$middlename = $this->input->post('middlename');
$lastname = $this->input->post('lastname');
$email = $this->input->post('email');
$groups_id = $this->input->post('groups_id');
$active = $this->input->post('active');
$confirm_password = '';
$password = '';
} else if (isset($user)) {
$username = $user->username;
$address = $user->address;
$phone = $user->phone;
$firstname = $user->firstname;
$middlename = $user->middlename;
$lastname = $user->lastname;
$email = $user->email;
$groups_id = $user->groups_id;
$active = $user->active;
$confirm_password = '';
$password = '';
} else {
$username = '';
$address = '';
$phone = '';
$firstname = '';
$middlename = '';
$lastname = '';
$email = '';
$groups_id = '';
$active = '';
$confirm_password = '';
$password = '';
$newform = 'yes';
}
?> 
<div class="row">
    <div class="col-lg-12">
        <?php if($this->session->flashdata('success')!=''){ ?>
        <div class="alert alert-success">
            <?php echo $this->session->flashdata('success'); ?>
             <button type="button" class="close" data-dismiss="alert" aria-label="close"><span aria-hidden="true">x</span></button>
        </div>
        <?php } ?>
        <?php if($this->session->flashdata('error')!=''){ ?>
        <div class="alert alert-danger">
            <?php echo $this->session->flashdata('error'); ?>
             <button type="button" class="close" data-dismiss="alert" aria-label="close"><span aria-hidden="true">x</span></button>
        </div>
        <?php } ?>
    </div>
    <div class="col-lg-12">
        <div class="mb-5">
            <form action=" " method="post" autocomplete="off" id="users-form">
                <div class="row">
                     <div class="col-lg-6 col-md-6">
                        <div class="form-group">
                            <label>Role</label>
                            <select class="form-control" name="groups_id">
                                <?php if (isset($groups) && count($groups) > 0) {
                                foreach ($groups as $group) {
                                    if($group->id!=13){
                                    ?>

                                <option value="<?php echo $group->id ?>" <?php echo ($groups_id == $group->id) ? 'selected' : '' ?>><?php echo $group->name ?></option>
                                <?php 
                                }
                                    }
                                }
                                ?>
                            </select>
                        </div>
                          <div class="form-group">
                            <label>First Name*</label>
                            <input type="text" class="form-control" placeholder="First Name" name="firstname" value="<?php echo $firstname ?>"  autocomplete='nope'>
                            <?php echo form_error('firstname', '<div class="error">', '</div>'); ?>
                        </div>
                   
                        <div class="form-group">
                            <label>Middle  Name</label>
                            <input type="text" class="form-control" placeholder="Middle Name" name="middlename" value="<?php echo $middlename ?>">
                            <?php echo form_error('middlename', '<div class="error">', '</div>'); ?>
                        </div>
                  
                        <div class="form-group">
                            <label>Last Name*</label>
                            <input type="text" class="form-control" placeholder="Last Name" name="lastname" value="<?php echo $lastname ?>">
                            <?php echo form_error('lastname', '<div class="error">', '</div>'); ?>
                        </div>
                  
                     
                   
                        <div class="form-group">
                            <label>Address</label>
                            <input type="text" class="form-control" placeholder="Address" name="address" value="<?php echo $address ?>" autocomplete="nope">
                            <?php echo form_error('address', '<div class="error">', '</div>'); ?>
                        </div>
                   
                    
                        <div class="form-group">
                            <label>Mobile Number*</label>
                            <input type="text" class="form-control" placeholder="Mobile Number" name="phone" value="<?php echo $phone ?>" autocomplete="nope">
                            <?php echo form_error('phone', '<div class="error">', '</div>'); ?>
                        </div>
                         <div class="form-group">
                            <input type="submit" class="btn btn-rounded btn-success mr-2 mb-2">
                            <a href="<?php echo base_url().'users'; ?>" class="btn btn-rounded btn-danger mr-2 mb-2">Cancel</a>
                        </div>
                    </div>

                     <div class="col-lg-6 col-md-6">
                       
                        
                           <div class="form-group">
                            <label>Username*</label>
                            <input type="text" class="form-control" placeholder="User Name" name="username" value="<?php echo $username ?>" <?php echo (isset($is_edit) && $is_edit) ? 'readonly' : '' ?> autocomplete="false"  style="background-color: #fff;">
                            <?php echo form_error('username', '<div class="error">', '</div>'); ?>
                        </div>
                   
                    <?php if (isset($is_edit) && !$is_edit) {?>
                 
                        <div class="form-group">
                            <label>Password*</label>
                            <input type="password" class="form-control" placeholder="Password" name="password" value="<?php echo $password ?>"  autocomplete="false" style="background-color: #fff;">
                            <?php echo form_error('password', '<div class="error">', '</div>'); ?>
                        </div>
                   
                        <div class="form-group">
                            <label>Confirm Password*</label>
                            <input type="password" class="form-control" placeholder="Confirm Password" name="confirm_password" value="<?php echo $confirm_password ?>"  autocomplete="false" readonly onfocus="this.removeAttribute('readonly');" style="background-color: #fff;">
                            <?php echo form_error('confirm_password', '<div class="error">', '</div>'); ?>
                        </div>
                  
                    <?php }?>
                      
                   
                        <div class="form-group">
                            <label>Email*</label>
                            <input type="text" class="form-control" placeholder="Email" name="email" value="<?php echo $email ?>">
                            <?php echo form_error('email', '<div class="error">', '</div>'); ?>
                        </div>
                   
                       
                     <div class="form-group">
                            <label>Status</label>
                            <select class="form-control" name="active">
                                <option value="0"   <?php echo ($active == 0) ? "selected" : "" ?>  >Inactive</option>
                                <option value="1" <?php echo (isset($newform) && $newform =='yes')?"selected":"" ?>  <?php echo ($active == 1) ? "selected" : "" ?>>Active</option>
                            </select>
                        </div>
                    
                   
                       
                 </div>
                </div>
            </form>
        </div>
    </div>
</div>
<script type="text/javascript">
//  $(document).ready(function(){
// $('#users-form').on( 'focus', ':input', function(){
// $(this).attr( 'autocomplete', 'off' );
// });
// });


</script>