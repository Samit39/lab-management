<?php

class Login_model extends MY_Model {

	public function __construct()
    {
        parent::__construct();
        $this->load->model('common/emailtemplate_model', 'emailtemplate');
        $this->load->model('common/sitesettings_model', 'sitesettings');
    }

	public function check_login($username,$password)
	{
		$this->db->select('t1.*');
		$this->db->from('tbl_users t1');
		$this->db->where('username',$username);
		$this->db->where('password',md5($password));
		$query = $this->db->get();
		if($query->num_rows()>0)
		{
			$session_data = array(
				'is_logged_in' => true,
				'user_id' => $query->result()[0]->id,
				'group_id' => $query->result()[0]->groups_id,
				'username' => $query->result()[0]->username,
				'full_name' => $query->result()[0]->firstname . " ". $query->result()[0]->middlename . " " . $query->result()[0]->lastname,
				'email' => $query->result()[0]->email,
				'user_created_date' => $query->result()[0]->created,
				'accessible_module' => $this->getAccessibleModulesForThisGroup($query->result()[0]->groups_id)
			);
			$this->session->set_userdata($session_data);
			$user_data = $this->session->all_userdata();
			return true;
		}
		else
		{
			// echo "invalid";
			return false;
		}

	}

	public function check_first_login($username)
	{
		$this->db->select('t1.*');
		$this->db->from('tbl_users t1');
		$this->db->where('username',$username);
		$query = $this->db->get();
		if($query->num_rows()>0)
		{
			if($query->result()[0]->first_login == 0)
			{
				return "Yes";
			}
			else
			{
				return "No";
			}
		}
		else
		{
			return false;
		}
	}

	public function check_valid_email($email)
	{
		$this->db->select('t1.*');
		$this->db->from('tbl_users t1');
		$this->db->where('email',$email);
		$query = $this->db->get();
		if($query->num_rows()>0)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	public function reset_password_and_email($email)
	{
		$new_password =  generate_random_string(8); 
		$data['password'] = md5($new_password);
		$this->update_tbl_users_by_email($email,$data);
		$all_information_for_this_user = $this->getAllInformationByEmail($email);
		//Sending Email Starts Here
		$sitesettings_information = $this->sitesettings->getSiteSettings();
		$registration_email_raw = $this->emailtemplate->getEmailTemplateByName('reset_password');
		$user_replace = array(
          'name' => $all_information_for_this_user->firstname . " " . $all_information_for_this_user->middlename . " " . $all_information_for_this_user->lastname,
          'new_password' => $new_password,
          'link' => base_url() 
        );
        $user_email = parse_email_template($registration_email_raw->userMessage, $user_replace);
        $email_parameter_array = array(
          'from' => $sitesettings_information->site_email,
          'fromname' => $sitesettings_information->site_title,
          'to' => $email,
          'toname' => $sitesettings_information->site_title,
          'subject' => $registration_email_raw->userSubject,
          'message' => $user_email
        );
        $mail_send_type = $sitesettings_information->mail_send_type;
        if($mail_send_type=="SWIFTSEND")
        {
        	swiftsend($email_parameter_array);
        }
        else if($mail_send_type=="PHPNATIVEMAIL")
        {
        	$this->sendNativeMailInitial($email_parameter_array);
        }
		//Sending Email Ends Here
	}

	public function update_password($user_id,$new_password)
	{
		$data['password'] = md5($new_password);
		$data['pwd_change_on'] = date('Y-m-d H:i:s', time());
		$data['first_login'] = 1;
		$this->db->where('id', $user_id);
		$this->db->update('tbl_users', $data);
	}

	public function update_tbl_users_by_email($email,$data)
	{
		$this->db->where('email', $email);
		$this->db->update('tbl_users', $data);
	}

	public function getAllInformationByEmail($email)
	{
		$this->db->select('t1.*');
		$this->db->from('tbl_users t1');
		$this->db->where('email',$email);
		$query = $this->db->get();
		if($query->num_rows()>0)
		{
			return $query->result()[0];
		}
		else
		{
			return false;
		}
	}

	public function getAccessibleModulesForThisGroup($group_id)
	{
		$accessible_module_string = '';
		$this->db->select('t1.*');
		$this->db->from('tbl_group_permission t1');
		$this->db->where('group_id',$group_id);
		$query = $this->db->get();
		if($query->num_rows()>0)
		{
			foreach ($query->result() as $value) 
			{
				$accessible_module_string = $accessible_module_string . $value->permission_id . ",";
			}
			return $accessible_module_string;
		}
		else
		{
			return false;
		}	
	}
}