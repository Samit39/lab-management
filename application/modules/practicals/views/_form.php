<div class="row">
    <div class="col-lg-12">
        <div class="mb-5">
            <?php if($this->session->flashdata('success')!=''){ ?>
            <div class="alert alert-success">
                <strong>Success!!! </strong><?php echo $this->session->flashdata('success'); ?>
            </div>
            <?php } ?>
            <?php if($this->session->flashdata('error')!=''){ ?>
            <div class="alert alert-danger">
                <strong>Error!!! </strong><?php echo $this->session->flashdata('error'); ?>
            </div>
            <?php } ?>
            <?php
            // pr($practicals_data);
            if(isset($practical_id) && $practical_id != '')
            {
                $form_submit_url = base_url().'practicals/create/'.$practical_id;
            }
            else
            {
                $form_submit_url = base_url().'practicals/create';
            } 
            ?>
            <?php //pr($practicals_data); echo $practicals_data[0]->name; ?>
            <form id="practicals_formsubmit" action="<?php echo $form_submit_url; ?>" method="POST" autocomplete="off">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="form-group">
                            <label>Select Lab: *</label>
                            <select class="form-control" id ="lab_select" name="lab_select" required>
                                <option value="">--Select Lab--</option>
                            <?php
                            if(isset($labs_result) && !empty($labs_result))
                            {
                                foreach ($labs_result as $value) 
                                {
                                ?>
                                    <option <?php echo (isset($practicals_data) && $practicals_data[0]->lab_id == $value->id) ? "selected" : '' ;?> value="<?php echo $value->id; ?>"><?php echo $value->title;?></option>
                                <?php
                                } //for each ends
                            } //if ends
                            ?>
                            </select>
                            <?php echo form_error("lab_select"); ?>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="form-group">
                            <label>Select Practical Date: *</label>
                            <input type="text" class="form-control" placeholder="Select Practical Date" id="practical_date" name="practical_date" value="<?php echo(isset($practicals_data) && $practicals_data[0]->practical_date!='') ? $practicals_data[0]->practical_date : '' ?>">
                            <?php echo form_error("practical_date"); ?>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="form-group">
                            <label>Practical Day: *</label>
                            <input type="text" class="form-control" placeholder="Practical Day" id="practical_day" name="practical_day" value="<?php echo(isset($practicals_data) && $practicals_data[0]->practical_day!='') ? $practicals_data[0]->practical_day : '' ?>" readonly>
                            <?php echo form_error("practical_day"); ?>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="form-group">
                            <label>Practical Name: *</label>
                            <input type="text" class="form-control" placeholder="Practical Name" id="practical_name" name="practical_name" value="<?php echo(isset($practicals_data) && $practicals_data[0]->practical_name!='') ? $practicals_data[0]->practical_name : '' ?>">
                            <?php echo form_error("practical_name"); ?>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="form-group">
                            <label>Practical Teacher: *</label>
                            <input type="text" class="form-control" placeholder="Practical Teacher" id="practical_teacher" name="practical_teacher" value="<?php echo(isset($practicals_data) && $practicals_data[0]->practical_teacher!='') ? $practicals_data[0]->practical_teacher : '' ?>">
                            <?php echo form_error("practical_teacher"); ?>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="form-group">
                            <label>Practical Group: *</label>
                            <input type="text" class="form-control" placeholder="Practical Group" id="practical_group" name="practical_group" value="<?php echo(isset($practicals_data) && $practicals_data[0]->practical_group!='') ? $practicals_data[0]->practical_group : '' ?>">
                            <?php echo form_error("practical_group"); ?>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="form-group">
                            <label>Practical Class: *</label>
                            <input type="text" class="form-control" placeholder="Practical Class" id="practical_class" name="practical_class" value="<?php echo(isset($practicals_data) && $practicals_data[0]->practical_class!='') ? $practicals_data[0]->practical_class : '' ?>">
                            <?php echo form_error("practical_class"); ?>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="form-group">
                            <label>Practical Section: *</label>
                            <input type="text" class="form-control" placeholder="Practical Section" id="practical_section" name="practical_section" value="<?php echo(isset($practicals_data) && $practicals_data[0]->practical_section!='') ? $practicals_data[0]->practical_section : '' ?>">
                            <?php echo form_error("practical_section"); ?>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="form-group">
                            <input type="submit" class="btn btn-rounded btn-success mr-2 mb-2">
                            <a href="<?php echo base_url().'practicals'; ?>" class="btn btn-rounded btn-danger mr-2 mb-2">Cancel</a>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>