<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Practicals extends MY_Controller {

	public function __construct() {
        parent::__construct();
        $this->permission_name = "Practicals";
        // is this module accessible for this group check_permission();
		//
		if(!$this->check_permission($this->permission_name))
		{
			$this->session->set_flashdata('error', 'Sorry You Dont Have Necessary Permission!');
			redirect('dashboard');
		}
        $this->load->model('practicals_model', 'practicals');
    }

	public function index()
	{   
		$data = array(); 
		$this->load->library('form_validation');
		$this->list_page($data);
	}

	public function search()
	{
		$data['labs_result'] = $this->practicals->getAllActiveLabs();
		$post = $_POST;
		if($post['lab_select'] != '')
		{
			$data['information_list'] = $this->practicals->getAllInformationByLabID($post['lab_select']);
			$data['lab_id'] = $post['lab_select'];
			// print_r($data['information_list']);
			// die();
		}
		$this->list_page($data);
	}

	public function list_page($data)
	{
		$data['breadcrumbs_title'] = "Practicals Manager";
		$data['content'] = '_list';
		// $data['information_list'] = $this->practicals->getAllInformation();
		$data['labs_result'] = $this->practicals->getAllActiveLabs();
		$this->load->view('common/header');
		$this->load->view('common/body',$data);
	}

	public function create()
	{
		$practical_id = segment(3);
		$post = $_POST;
		$user_data = $this->session->all_userdata();
		//
		// $data['users_list'] = $this->practicals->getAllUsersList();
		//
		if($practical_id!='')
		{
			$data['practicals_data'] = $this->practicals->get_detail_by_id($practical_id);
			$data['practical_id'] = $practical_id;
			// pr($data['practicals_data']);
			// echo $data['practicals_data']->name;
			// die();
		}
		if(!empty($post))
		{ 
			// $this->form_validation->set_rules($this->practicals->rules());


			// if ($this->form_validation->run() == true)
			// {

			if($practical_id!='')
			{
				//EDIT ko lagi
				// Data For tbl_practicals
				$tbl_practicals_data['lab_id'] = $post['lab_select'];
				$tbl_practicals_data['practical_date'] = $post['practical_date'];
				$tbl_practicals_data['practical_day'] = $post['practical_day'];
				$tbl_practicals_data['practical_name'] = $post['practical_name'];
				$tbl_practicals_data['practical_teacher'] = $post['practical_teacher'];
				$tbl_practicals_data['practical_group'] = $post['practical_group'];
				$tbl_practicals_data['practical_class'] = $post['practical_class'];
				$tbl_practicals_data['practical_section'] = $post['practical_section'];
				$tbl_practicals_data['updated_date'] = date("Y-m-d h:i:s");
				$tbl_practicals_data['status'] = 1;
				$tbl_practicals_data['updated_by'] = $user_data['user_id'];
				//
				$this->practicals->update_information($practical_id,$tbl_practicals_data);
				//
				$this->session->set_flashdata('success', 'Information Successfully Edited In Database.');
				redirect('practicals');
			}
			else
			{
				//Add ko lagi
				// Data For tbl_practicals
				$tbl_practicals_data['lab_id'] = $post['lab_select'];
				$tbl_practicals_data['practical_date'] = $post['practical_date'];
				$tbl_practicals_data['practical_day'] = $post['practical_day'];
				$tbl_practicals_data['practical_name'] = $post['practical_name'];
				$tbl_practicals_data['practical_teacher'] = $post['practical_teacher'];
				$tbl_practicals_data['practical_group'] = $post['practical_group'];
				$tbl_practicals_data['practical_class'] = $post['practical_class'];
				$tbl_practicals_data['practical_section'] = $post['practical_section'];
				$tbl_practicals_data['created_date'] = date("Y-m-d h:i:s");
				$tbl_practicals_data['updated_date'] = date("Y-m-d h:i:s");
				$tbl_practicals_data['status'] = 1;
				$tbl_practicals_data['updated_by'] = $user_data['user_id'];
				//
				$inserted_practicals_id = $this->practicals->insert_new_information($tbl_practicals_data);
				//
				$this->session->set_flashdata('success', 'Information Successfully Added To Database.');
				redirect('practicals');
			}
			// } //Form Validation	
		}
		$data['labs_result'] = $this->practicals->getAllActiveLabs();
		$data['breadcrumbs_title'] = "Add Practical Information";
		$data['content'] = '_form';
		$data['addJs'] = array('assets/js/practicals.js');
		$this->load->view('common/header');
		$this->load->view('common/body',$data);	
	}

	public function delete()
	{
		$practicals_id = segment(3);
		if($practicals_id !='')
		{
			$this->practicals->delete_practicals($practicals_id);
			$this->session->set_flashdata('success', 'Information Successfully Deleted From Database.');
		}
		else
		{
			$this->session->set_flashdata('error', 'Parameter error while deleting group.');
		}
		redirect('practicals');
	}

	//Start Of Recycle Bin From Here
	public function recycle_bin_list()
	{
		$data['breadcrumbs_title'] = "practicals Recycle Bin";
		$data['content'] = '_recycle_bin_list';
		$data['information_list'] = $this->practicals->getAllInactiveInformation();
		// pr($data['information_list']);
		// die();
		// $data['addJs'] = array('assets/js/abc.js');
		// $data['addCss'] = array('assets/js/abc.js');
		$this->load->view('common/header');
		$this->load->view('common/body',$data);
	}

	public function recycle()
	{
		$practicals_id = segment(3);
		if($practicals_id !='')
		{
			$this->practicals->recycle_practicals($practicals_id);
			$this->session->set_flashdata('success', 'Information Successfully Recycled And Is Now Available In practicals.');
		}
		else
		{
			$this->session->set_flashdata('error', 'Parameter error while deleting group.');
		}
		redirect('practicals/recycle_bin_list');
	}
}
