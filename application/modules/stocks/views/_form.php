<div class="row">
    <div class="col-lg-12">
        <div class="mb-5">
            <?php if($this->session->flashdata('success')!=''){ ?>
            <div class="alert alert-success">
                <strong>Success!!! </strong><?php echo $this->session->flashdata('success'); ?>
            </div>
            <?php } ?>
            <?php if($this->session->flashdata('error')!=''){ ?>
            <div class="alert alert-danger">
                <strong>Error!!! </strong><?php echo $this->session->flashdata('error'); ?>
            </div>
            <?php } ?>
            <?php
            // pr($stocks_data);
            if(isset($stock_id) && $stock_id != '')
            {
                $form_submit_url = base_url().'stocks/create/'.$stock_id;
            }
            else
            {
                $form_submit_url = base_url().'stocks/create';
            } 
            ?>
            <?php //pr($stocks_data); echo $stocks_data[0]->name; ?>
            <form id="stocks_formsubmit" action="<?php echo $form_submit_url; ?>" method="POST" autocomplete="off">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="form-group">
                            <label>Select Lab: *</label>
                            <select class="form-control" id ="lab_select" name="lab_select" required>
                                <option value="">--Select Lab--</option>
                            <?php
                            if(isset($labs_result) && !empty($labs_result))
                            {
                                foreach ($labs_result as $value) 
                                {
                                ?>
                                    <option <?php echo (isset($stocks_data) && $stocks_data[0]->lab_id == $value->id) ? "selected" : '' ;?> value="<?php echo $value->id; ?>"><?php echo $value->title;?></option>
                                <?php
                                } //for each ends
                            } //if ends
                            ?>
                            </select>
                            <?php echo form_error("lab_select"); ?>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="form-group">
                            <label>Item Code:</label>
                            <input type="text" class="form-control" placeholder="Item Code" id="stock_item_code" name="stock_item_code" value="<?php echo(isset($stocks_data) && $stocks_data[0]->stock_item_code!='') ? $stocks_data[0]->stock_item_code : '' ?>">
                            <?php echo form_error("stock_item_code"); ?>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="form-group">
                            <label>Item Name: *</label>
                            <input type="text" class="form-control" placeholder="Item Name" id="stock_item_name" name="stock_item_name" value="<?php echo(isset($stocks_data) && $stocks_data[0]->stock_item_name!='') ? $stocks_data[0]->stock_item_name : '' ?>" required>
                            <?php echo form_error("stock_item_name"); ?>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="form-group">
                            <label>Stock Count: *</label>
                            <input type="number" class="form-control" min="1" placeholder="Stock Count" id="stock_item_count" name="stock_item_count" value="<?php echo(isset($stocks_data) && $stocks_data[0]->stock_item_count!='') ? $stocks_data[0]->stock_item_count : '' ?>" required>
                            <?php echo form_error("stock_item_count"); ?>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="form-group">
                            <label>Description:</label>
                            <input type="text" class="form-control" placeholder="Description" id="stock_item_description" name="stock_item_description" value="<?php echo(isset($stocks_data) && $stocks_data[0]->stock_item_description!='') ? $stocks_data[0]->stock_item_description : '' ?>">
                            <?php echo form_error("stock_item_description"); ?>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="form-group">
                            <input type="submit" class="btn btn-rounded btn-success mr-2 mb-2">
                            <a href="<?php echo base_url().'stocks'; ?>" class="btn btn-rounded btn-danger mr-2 mb-2">Cancel</a>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>