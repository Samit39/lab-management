<div class="row">
    <div class="col-lg-12">
        <?php if($this->session->flashdata('success')!=''){ ?>
        <div class="alert alert-success">
            <strong>Success!!! </strong><?php echo $this->session->flashdata('success'); ?>
        </div>
        <?php } ?>
        <?php if($this->session->flashdata('error')!=''){ ?>
        <div class="alert alert-danger">
            <strong>Error!!! </strong><?php echo $this->session->flashdata('error'); ?>
        </div>
        <?php } ?>
        <div class="mb-5" style="margin-top: 55px;">
            <table class="table table-hover display" id="recycle_bin_frontdesk_table">
                <thead>
                    <tr>
                        <th >SN</th>
                        <th >Name</th>
                        <th >Contact Number</th>
                        <th >Email</th>
                        <th >Qualification</th>
                        <th >Date Of Birth</th>
                        <th >Remarks</th>
                        <th >Updated Date</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    if(isset($information_list) && !empty($information_list))
                    {
                    $i = 1;
                    foreach ($information_list as $value) {
                    ?>
                    <tr>
                        <td><?php echo $i; ?></td>
                        <td><?php echo $value->name; ?></td>
                        <td><?php echo $value->contact_number; ?></td>
                        <td><?php echo $value->email; ?></td>
                        <td><?php echo $value->qualification; ?></td>
                        <td><?php echo $value->dob; ?></td>
                        <td><?php echo $value->remarks; ?></td>
                        <td><?php echo $value->updated_date; ?></td>
                        <td>
                            <div class="btn-group mb-2 table-action-icon-group" aria-label="" role="group">
                                <a href="<?php echo base_url() . 'frontdesk/recycle/' . $value->id; ?>" class="btn btn-success table-action-icon"><i class="fa fa-recycle" aria-hidden="true"></i></a>
                            </div>
                        </td>
                    </tr>
                    <?php
                    $i++;
                    } //for each loop
                    } //if condition
                    ?>
                </tbody>
                <!-- <tfoot>
                <tr>
                    <th>Name</th>
                    <th>Position</th>
                    <th>Office</th>
                    <th>Age</th>
                    <th>Date</th>
                    <th>Salary</th>
                </tr>
                </tfoot> -->
            </table>
        </div>
    </div>
</div>
<script>
(function($) {
"use strict";
$(function () {
    var top_message = "The information in this table is copyright to Global Journey Education Foundation.";
    var file_title = "Deleted Frontdesk Information List";
    $('#recycle_bin_frontdesk_table').DataTable({
        responsive: true,
        dom: 'Bfrtip',
        buttons: [
            'copy',
            {
                extend: 'excel',
                messageTop: top_message,
                title: file_title
            },
            {
                extend: 'csv',
                messageTop: top_message,
                title: file_title
            },
            {
                extend: 'pdf',
                messageTop: top_message,
                title: file_title
            },
            {
                extend: 'print',
                messageTop: top_message,
                title: file_title
            }
        ]
    });
/*
$('#example2').DataTable({
autoWidth: true,
scrollX: true,
fixedColumns: true
});
$('#example3').DataTable({
autoWidth: true,
scrollX: true,
fixedColumns: true
});
*/
});
})(jQuery)
</script>