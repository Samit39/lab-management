<?php

class Groups_model extends MY_Model {

    public $table = 'groups';
    public $id = '',$name = '',$description='';

    public function __construct() {
        parent::__construct();
      
    }

    public function rules($id = '') {
        $array = array(
            array(
                'field' => 'name',
                'label' => 'name',
                'rules' => 'trim|required',
            ),
             array(
                'field' => 'description',
                'label' => 'description',
                'rules' => 'trim|required',
            ),

 
        );

        return $array;
    }

	public function getAllGroups()
	{
		$final_result = array();
		$this->db->select('t1.*');
		$this->db->from('tbl_groups t1');
		$query = $this->db->get();
		if($query->num_rows()>0)
		{
			// return $query->result();
			foreach ($query->result() as $value) {
				$value->users = $this->getUsersCount($value->id);
				array_push($final_result,$value);
			}
			return $final_result;
		}
		else
		{
			return false;
		}
	}
	public function getAllGroupsinuser()
	{
		$final_result = array();
		$this->db->select('t1.*');
		$this->db->from('tbl_groups t1');
		$this->db->where('active','1');
		$query = $this->db->get();
		if($query->num_rows()>0)
		{
			// return $query->result();
			foreach ($query->result() as $value) {
				$value->users = $this->getUsersCount($value->id);
				array_push($final_result,$value);
			}
			return $final_result;
		}
		else
		{
			return false;
		}
	}


	public function getUsersCount($group_id)
	{
		$this->db->select('t1.*');
		$this->db->from('tbl_users t1');
		$this->db->where('t1.groups_id',$group_id);
		$query = $this->db->get();
		if($query->num_rows() > 0)
		{
			return $query->num_rows();
		}
		else
		{
			return 0;
		}
	}

	public function insert_new_group($data)
	{
		$this->db->insert('tbl_groups', $data);
		$insert_id = $this->db->insert_id();
		return  $insert_id;
	}

	public function get_detail_by_id($group_id)
	{
		$this->db->select('t1.*');
		$this->db->from('tbl_groups t1');
		$this->db->where('t1.id',$group_id);
		$query = $this->db->get();
		if($query->num_rows()>0)
		{
			return $query->row();
		}
		else
		{
			return false;
		}
	}

	public function update_group($group_id,$data)
	{
		$this->db->where('id', $group_id);
		$this->db->update('tbl_groups', $data);
	}

	public function delete_group($group_id)
	{
		$this->db->where('id', $group_id);
		$this->db->delete('tbl_groups');
	}
	 public function change_status_of_groups($group_id)
    {
        $this->db->select('t1.*');
        $this->db->from('tbl_groups t1');
        $this->db->where('t1.id',$group_id);
        $query = $this->db->get();
        if ($query->num_rows() > 0) 
        {
            $user_current_status = $query->result()[0]->active;
            if($user_current_status==0)
            {
                $data['active'] = 1;
            }
            else
            {
                $data['active'] = 0;
            }
            $this->db->where('id', $group_id);
            $this->db->update('tbl_groups', $data);
        } 
        else 
        {
            return false;
        }
    }

    public function getAllUsersInThisGroup($group_id)
    {
    	$this->db->select('t1.*');
        $this->db->from('tbl_users t1');
        $this->db->where('t1.groups_id',$group_id);
        $query = $this->db->get();
        if ($query->num_rows() > 0) 
        {
        	return $query->result();
        }
        else
        {
        	return false;
        }
    }
}