<div class="row">
    <div class="col-lg-12">
        <div class="mb-5">
            <?php if($this->session->flashdata('success')!=''){ ?>
            <div class="alert alert-success">
                <strong>Success!!! </strong><?php echo $this->session->flashdata('success'); ?>
            </div>
            <?php } ?>
            <?php if($this->session->flashdata('error')!=''){ ?>
            <div class="alert alert-danger">
                <strong>Error!!! </strong><?php echo $this->session->flashdata('error'); ?>
            </div>
            <?php } ?>
            <?php
            // pr($labs_data);
            if(isset($lab_id) && $lab_id != '')
            {
                $form_submit_url = base_url().'labs/create/'.$lab_id;
            }
            else
            {
                $form_submit_url = base_url().'labs/create';
            } 
            ?>
            <?php //pr($labs_data); echo $labs_data[0]->name; ?>
            <form id="labs_formsubmit" action="<?php echo $form_submit_url; ?>" method="POST" autocomplete="off">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="form-group">
                            <label>Title: *</label>
                            <input type="text" class="form-control" placeholder="Title" id="title" name="title" value="<?php echo(isset($labs_data) && $labs_data[0]->title!='') ? $labs_data[0]->title : '' ?>" required>
                            <?php echo form_error("title"); ?>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="form-group">
                            <label>Description: *</label>
                            <input type="text" class="form-control" placeholder="Description" id="description" name="description" value="<?php echo(isset($labs_data) && $labs_data[0]->description!='') ? $labs_data[0]->description : '' ?>" required>
                            <?php echo form_error("description"); ?>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="form-group">
                            <input type="submit" class="btn btn-rounded btn-success mr-2 mb-2">
                            <a href="<?php echo base_url().'labs'; ?>" class="btn btn-rounded btn-danger mr-2 mb-2">Cancel</a>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>