<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Labs extends MY_Controller {

	public function __construct() {
        parent::__construct();
        $this->permission_name = "Labs";
        // is this module accessible for this group check_permission();
		//
		if(!$this->check_permission($this->permission_name))
		{
			$this->session->set_flashdata('error', 'Sorry You Dont Have Necessary Permission!');
			redirect('dashboard');
		}
        $this->load->model('labs_model', 'labs');
    }

	public function index()
	{   
		$this->load->library('form_validation');
		$data = array(); 
		$data['breadcrumbs_title'] = "Lab Manager";
		$data['content'] = '_list';
		$data['information_list'] = $this->labs->getAllInformation();
		$this->load->view('common/header');
		$this->load->view('common/body',$data);
	}

	public function create()
	{
		$lab_id = segment(3);
		$post = $_POST;
		$user_data = $this->session->all_userdata();
		//
		// $data['users_list'] = $this->labs->getAllUsersList();
		//
		if($lab_id!='')
		{
			$data['labs_data'] = $this->labs->get_detail_by_id($lab_id);
			$data['lab_id'] = $lab_id;
			// pr($data['labs_data']);
			// echo $data['labs_data']->name;
			// die();
		}
		if(!empty($post))
		{

			// $this->form_validation->set_rules($this->labs->rules());


			// if ($this->form_validation->run() == true)
			// {

			if($lab_id!='')
			{
				//EDIT ko lagi
				// Data For tbl_labs
				$tbl_labs_data['title'] = $post['title'];
				$tbl_labs_data['description'] = $post['description'];
				$tbl_labs_data['updated_date'] = date("Y-m-d h:i:s");
				$tbl_labs_data['status'] = 1;
				$tbl_labs_data['updated_by'] = $user_data['user_id'];
				//
				$this->labs->update_information($lab_id,$tbl_labs_data);
				//
				$this->session->set_flashdata('success', 'Information Successfully Edited In Database.');
				redirect('labs');
			}
			else
			{
				//Add ko lagi
				// Data For tbl_labs
				$tbl_labs_data['title'] = $post['title'];
				$tbl_labs_data['description'] = $post['description'];
				$tbl_labs_data['created_date'] = date("Y-m-d h:i:s");
				$tbl_labs_data['updated_date'] = date("Y-m-d h:i:s");
				$tbl_labs_data['status'] = 1;
				$tbl_labs_data['updated_by'] = $user_data['user_id'];
				//
				$inserted_labs_id = $this->labs->insert_new_information($tbl_labs_data);
				//
				$this->session->set_flashdata('success', 'Information Successfully Added To Database.');
				redirect('labs');
			}
			// } //Form Validation	
		}
		$data['breadcrumbs_title'] = "Add Information";
		$data['content'] = '_form';
		$data['addJs'] = array('assets/js/labs.js');
		$this->load->view('common/header');
		$this->load->view('common/body',$data);	
	}

	public function delete()
	{
		$labs_id = segment(3);
		if($labs_id !='')
		{
			$this->labs->delete_labs($labs_id);
			$this->session->set_flashdata('success', 'Information Successfully Deleted From Database.');
		}
		else
		{
			$this->session->set_flashdata('error', 'Parameter error while deleting group.');
		}
		redirect('labs');
	}

	//Start Of Recycle Bin From Here
	public function recycle_bin_list()
	{
		$data['breadcrumbs_title'] = "labs Recycle Bin";
		$data['content'] = '_recycle_bin_list';
		$data['information_list'] = $this->labs->getAllInactiveInformation();
		// pr($data['information_list']);
		// die();
		// $data['addJs'] = array('assets/js/abc.js');
		// $data['addCss'] = array('assets/js/abc.js');
		$this->load->view('common/header');
		$this->load->view('common/body',$data);
	}

	public function recycle()
	{
		$labs_id = segment(3);
		if($labs_id !='')
		{
			$this->labs->recycle_labs($labs_id);
			$this->session->set_flashdata('success', 'Information Successfully Recycled And Is Now Available In labs.');
		}
		else
		{
			$this->session->set_flashdata('error', 'Parameter error while deleting group.');
		}
		redirect('labs/recycle_bin_list');
	}
}
