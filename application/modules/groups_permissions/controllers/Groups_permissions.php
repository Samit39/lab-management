<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Groups_permissions extends MY_Controller {

	public function __construct() {
        parent::__construct();
        $this->permission_name = "Groups Permissions";
        // is this module accessible for this group check_permission();
        //
        // if(!$this->check_permission($this->permission_name))
        // {
        //     $this->session->set_flashdata('error', 'Sorry You Dont Have Necessary Permission!');
        //     redirect('dashboard');
        // }
        $this->load->model('groups_permissions_model', 'groups_permissions');
        
    }

	public function index()
	{
		$this->load->library('form_validation');
		$data = array();
		$data['breadcrumbs_title'] = "Groups Permissions";
		$data['content'] = '_list';
		$data['groups_permissions_list'] = $this->groups_permissions->getAllgroups_permissions();
		// pr($data['groups_permissions_list']);
		// die();
		// $data['addJs'] = array('assets/js/groups_permissions.js');
		// $data['addCss'] = array('assets/js/abc.js');
		$this->load->view('common/header');
		$this->load->view('common/body',$data);
	}

	public function create()
	{
		$groups_permissions_id = segment(3);
		$post = $_POST;
		if($groups_permissions_id!='')
		{
			
			$data['groups_permissions_data'] = $this->groups_permissions->get_detail_by_id($groups_permissions_id);
			$data['groups_permissions_id'] = $groups_permissions_id;
			// pr($data['groups_permissions_data']);
			// echo $data['groups_permissions_data'][0]->name;
			// die();
			
		}
		if(!empty($post))
		{
			// $this->form_validation->set_rules($this->groups_permissions->rules());

		// if ($this->form_validation->run() == true)
		// {

			if($groups_permissions_id!='')
			{
			
				//EDIT ko lagi

				// pr($post);
				$this->groups_permissions->delete_group_permissions($groups_permissions_id);
				for ($i=0; $i < count($post['permissions']) ; $i++) 
				{ 
					$tbl_group_permission_data['group_id'] = $groups_permissions_id;
					$tbl_group_permission_data['permission_id'] =$post['permissions'][$i];
					$this->groups_permissions->insert_groups_permissions($tbl_group_permission_data);
				}
				// pr($tbl_group_permission_data);
				// die();
								
				
				$this->session->set_flashdata('success', 'Groups Permissions Successfully Edited.');
				redirect('groups_permissions');
			}
			/*
			else
			{

				//Add ko lagi
				$this->groups_permissions->insert_new_groups_permissions($post);
				$this->session->set_flashdata('success', 'groups_permissions Successfully Added.');
				redirect('groups_permissions');
			}
			*/
		// } // for form validation
	}
		$data['permissions_list'] = $this->groups_permissions->getAllPermissionsLists();	
		$data['breadcrumbs_title'] = "Edit Groups Permissions";
		$data['content'] = '_form';
		// $data['addJs'] = array('assets/js/groups_permissions.js');
		$this->load->view('common/header');
		$this->load->view('common/body',$data);	
		

	}

	/*
	public function delete()
	{
		$groups_permissions_id = segment(3);
		if($groups_permissions_id !='')
		{
			$this->groups_permissions->delete_groups_permissions($groups_permissions_id);
			$this->session->set_flashdata('success', 'groups_permissions Successfully Deleted.');
		}
		else
		{
			$this->session->set_flashdata('error', 'Parameter error while deleting groups_permissions.');
		}
		redirect('groups_permissions');
	}
	*/
}
