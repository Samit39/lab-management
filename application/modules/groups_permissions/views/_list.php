<div class="row">
    <div class="col-lg-12">
        <?php if($this->session->flashdata('success')!=''){ ?>
        <div class="alert alert-success">
           <?php echo $this->session->flashdata('success'); ?>
             <button type="button" class="close" data-dismiss="alert" aria-label="close"><span aria-hidden="true">x</span></button>
        </div>
        <?php } ?>
        <?php if($this->session->flashdata('error')!=''){ ?>
        <div class="alert alert-danger">
            <?php echo $this->session->flashdata('error'); ?>
             <button type="button" class="close" data-dismiss="alert" aria-label="close"><span aria-hidden="true">x</span></button>
        </div>
        <?php } ?>
        <div class="mb-5">
            <table class="table table-hover display" id="permissions_table">
                <thead>
                    <tr>
                        <th >SN</th>
                        <th >Group Name</th>
                        <th >Permissions</th>
                        <th >Actions</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    if(isset($groups_permissions_list) && !empty($groups_permissions_list))
                    {
                    $i = 1;
                    foreach ($groups_permissions_list as $value) {
                    ?>
                    <tr>
                        <td><?php echo $i; ?></td>
                        <td><?php echo $value->name; ?></td>
                        <td><?php echo $value->permissions; ?></td>
                        <td>
                            <div class="btn-group mb-2 table-action-icon-group" aria-label="" role="group">
                                <a href="<?php echo base_url() . 'groups_permissions/create/' . $value->id; ?>" class="btn btn-success table-action-icon" title="Edit"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                            </div>
                        </td>
                    </tr>
                    <?php
                    $i++;
                    } //for each loop
                    } //if condition
                    ?>
                </tbody>
                <!-- <tfoot>
                <tr>
                    <th>Name</th>
                    <th>Position</th>
                    <th>Office</th>
                    <th>Age</th>
                    <th>Date</th>
                    <th>Salary</th>
                </tr>
                </tfoot> -->
            </table>
        </div>
    </div>
</div>
<script>
(function($) {
"use strict";
$(function () {
$('#permissions_table').DataTable({
responsive: true
});
/*
$('#example2').DataTable({
autoWidth: true,
scrollX: true,
fixedColumns: true
});
$('#example3').DataTable({
autoWidth: true,
scrollX: true,
fixedColumns: true
});
*/
});
})(jQuery)
</script>