<?php

class Emailtemplate_model extends MY_Model {

	public function __construct()
    {
        parent::__construct();
    }

    public function getEmailTemplateByName($name)
    {
    	$this->db->select('t1.*');
		$this->db->from('tbl_email_template t1');
		$this->db->where('name',$name);
		$query = $this->db->get();
		if($query->num_rows()>0)
		{
			return $query->result()[0];
		}
		else
		{
			return false;
		}
    }
}