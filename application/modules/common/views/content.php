<div class="row">
    <div class="col-lg-12">
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="utils__title utils__title--flat mb-3">
            <span class="text-uppercase font-size-16">your accounts (6)</span>
            <!-- <Button class="ml-3 btn btn-outline-default btn-sm">View All</Button> -->
        </div>
        <div class="row">
            <div class="col-lg-6">
                <a href="javascript: void(0);" class="card card--withShadow paymentAccount">
                    <div class="paymentAccount__icon">
                        <i class="lnr lnr-inbox"></i>
                    </div>
                    <span class="paymentAccount__number">US 4658-1678-7528</span>
                    <span class="paymentAccount__sum">$2,156.78</span>
                    <div class="paymentAccount__footer">Current month charged: $10,200.00</div>
                </a>
            </div>
            <div class="col-lg-6">
                <a href="javascript: void(0);" class="card card--withShadow paymentAccount">
                    <div class="paymentAccount__icon">
                        <i class="lnr lnr-inbox"></i>
                    </div>
                    <span class="paymentAccount__number">IBAN 445646-8748-4664-1678-5416</span>
                    <span class="paymentAccount__sum">$560,245.35</span>
                    <div class="paymentAccount__footer">Current month charged: $1,276.00</div>
                </a>
            </div>
            <div class="col-lg-6">
                <a href="javascript: void(0);" class="card card--withShadow paymentAccount">
                    <div class="paymentAccount__icon">
                        <i class="lnr lnr-inbox"></i>
                    </div>
                    <span class="paymentAccount__number">US 4658-1678-7528</span>
                    <span class="paymentAccount__sum">$2,156.78</span>
                    <div class="paymentAccount__footer">Current month charged: $10,200.00</div>
                </a>
            </div>
            <div class="col-lg-6">
                <a href="javascript: void(0);" class="card card--withShadow paymentAccount">
                    <div class="paymentAccount__icon">
                        <i class="lnr lnr-inbox"></i>
                    </div>
                    <span class="paymentAccount__number">IBAN 445646-8748-4664-1678-5416</span>
                    <span class="paymentAccount__sum">$560,245.35</span>
                    <div class="paymentAccount__footer">Current month charged: $1,276.00</div>
                </a>
            </div>
        </div>
    </div>
</div>