<?php

function pr($data)
{
    echo '<pre>';
    print_r($data);
    echo '</pre>';
}

function segment($segment)
{
    $ci = &get_instance();
    $value = $ci->uri->segment($segment);
    return $value;
}

function get_mac_address()
{
    // Turn on output buffering
    ob_start();
    //Get the ipconfig details using system commond
    system('ipconfig /all');
    // Capture the output into a variable
    $mycom = ob_get_contents();
    // Clean (erase) the output buffer
    ob_clean();
    $findme = "Physical";
    //Search the "Physical" | Find the position of Physical text
    $pmac = strpos($mycom, $findme);
    // Get Physical Address
    $mac = substr($mycom, ($pmac + 36), 17);
    //Display Mac Address
    return $mac;
}

function swiftsend($params)
{

    require_once APPPATH . 'third_party/swiftmailer/swift_required.php';

    try {
        $ci = &get_instance();
        $query = 'select * from tbl_smtp_details';
        $data = $ci->db->query($query);
        $smtp_details = $data->row();

        $transport = Swift_SmtpTransport::newInstance($smtp_details->host_name, $smtp_details->port_number, $smtp_details->encryption)
            ->setUsername($smtp_details->user_name)
            ->setPassword($smtp_details->password);

        // if($_SERVER['SERVER_NAME'] === 'localhost') {
        // Create the Transport
        // $transport = Swift_SmtpTransport::newInstance('smtp.gmail.com', 465, 'ssl')
        //     ->setUsername('animesh.shrestha@amniltech.com')
        //     ->setPassword('S3pt!^1986');
        // } else {
        //     $transport = Swift_SmtpTransport::newInstance('smtp.gmail.com', 465, 'ssl')
        //         ->setUsername('animesh.shrestha@amniltech.com')
        //         ->setPassword('S3pt!^1986');
        // }

        // Create the Mailer using your created Transport
        $mailer = Swift_Mailer::newInstance($transport);

        // Create a message
        if (!empty($params['attachment'])) {
            $message = Swift_Message::newInstance($params['subject'])
                ->setFrom(array($params['from'] => $params['fromname']))
                ->setTo(array($params['to'] => $params['toname']))
                ->setBody($params['message'], 'text/html')
                ->attach(Swift_Attachment::fromPath($params['attachment']));
        } else if (!empty($params['ccname'])) {
            $message = Swift_Message::newInstance($params['subject'])
                ->setFrom(array($params['from'] => $params['fromname']))
                ->setTo(array($params['to'] => $params['toname']))
                ->setCc('CAP@nicasiabank.com')
                ->setBody($params['message'], 'text/html');
        } else {

            $message = Swift_Message::newInstance($params['subject'])
                ->setFrom(array($params['from'] => $params['fromname']))
                ->setTo(array($params['to'] => $params['toname']))
                ->setBody($params['message'], 'text/html');
        }

        // Send the message
        $result = $mailer->send($message);
        return $result;
    } catch (Exception $e) {
        if (ENVIRONMENT == 'development') {
            pr($e);
        } else {
            //redirect();
        }
    }
}

function sendMail($subject, $user_email = null, $body, $file = null, $ccEmail = null)
{
    $site = $this->siteDetails();

    if (strpos($user_email, ',') !== false) {
        $user_email = explode(',', $user_email);
        $to_email = $user_email;
    } else {
        $to_email = $user_email;
    }

    $ci = get_instance();
    $ci->load->library('email');
    $body = $body;

    try {
//        $config = Array(
        //            'protocol' => 'smtp',
        //            'smtp_host' => 'smtp.mailtrap.io',
        //            'smtp_port' => 2525,
        //            'smtp_user' => 'e9f0a3adb008b7',
        //            'smtp_pass' => 'b8a52c4182787b',
        //            'crlf' => "\r\n",
        //            'newline' => "\r\n"
        //        );
        // $config['protocol'] = "smtp";
        // $config['smtp_host'] = "ssl://smtp.gmail.com";
        // $config['smtp_port'] = "465";
        // $config['smtp_user'] = "bkeshswap@gmail.com";
        // $config['smtp_pass'] = "swapswap";
        $config['charset'] = "utf-8";
        $config['mailtype'] = "html";
// $config['newline'] = "\r\n";

        $this->email->initialize($config);
        $this->email->from($site->site_email, $site->site_title);
        $this->email->to($to_email);
        $this->email->subject($subject);
        $this->email->message($body);
        if ($file) {
            foreach ($file as $row) {
                $this->email->attach($row);
            }
// $this->email->attach($file);
        }
        $result = $this->email->send();
        if ($result) {
            $data = 'success';
        } else {
            $data = "error";
        }
    } catch (Exception $e) {
        $result = $this->email->print_debugger();
        $data = 'error';
        $error_message = $e;
    }

    return ($data);
}

function getBrowser()
{
	$u_agent = $_SERVER['HTTP_USER_AGENT'];
    $bname = 'Unknown';
    $platform = 'Unknown';
    $version = "";

    //First get the platform?
    if (preg_match('/linux/i', $u_agent)) {
        $platform = 'linux';
    } elseif (preg_match('/macintosh|mac os x/i', $u_agent)) {
        $platform = 'mac';
    } elseif (preg_match('/windows|win32/i', $u_agent)) {
        $platform = 'windows';
    }

    // Next get the name of the useragent yes seperately and for good reason
    if (preg_match('/MSIE/i', $u_agent) && !preg_match('/Opera/i', $u_agent)) {
        $bname = 'Internet Explorer';
        $ub = "MSIE";
    } elseif (preg_match('/Firefox/i', $u_agent)) {
        $bname = 'Mozilla Firefox';
        $ub = "Firefox";
    } elseif (preg_match('/Chrome/i', $u_agent)) {
        $bname = 'Google Chrome';
        $ub = "Chrome";
    } elseif (preg_match('/Safari/i', $u_agent)) {
        $bname = 'Apple Safari';
        $ub = "Safari";
    } elseif (preg_match('/Opera/i', $u_agent)) {
        $bname = 'Opera';
        $ub = "Opera";
    } elseif (preg_match('/Netscape/i', $u_agent)) {
        $bname = 'Netscape';
        $ub = "Netscape";
    }

    // finally get the correct version number
    $known = array('Version', $ub, 'other');
    $pattern = '#(?<browser>' . join('|', $known) .
        ')[/ ]+(?<version>[0-9.|a-zA-Z.]*)#';
    if (!preg_match_all($pattern, $u_agent, $matches)) {
        // we have no matching number just continue
    }

    // see how many we have
    $i = count($matches['browser']);
    if ($i != 1) {
        //we will have two since we are not using 'other' argument yet
        //see if version is before or after the name
        if (strripos($u_agent, "Version") < strripos($u_agent, $ub)) {
            $version = $matches['version'][0];
        } else {
            $version = $matches['version'][1];
        }
    } else {
        $version = $matches['version'][0];
    }

    // check if we have a number
    if ($version == null || $version == "") {
        $version = "?";
    }
    $data = array(
        'userAgent' => $u_agent,
        'name' => $bname,
        'version' => $version,
        'platform' => ucwords($platform),
        'pattern' => $pattern,
    );
    return $data;
}

function get_ip_address() {
    $ipaddress = '';
    if (getenv('HTTP_CLIENT_IP'))
        $ipaddress = getenv('HTTP_CLIENT_IP');
    else if(getenv('HTTP_X_FORWARDED_FOR'))
        $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
    else if(getenv('HTTP_X_FORWARDED'))
        $ipaddress = getenv('HTTP_X_FORWARDED');
    else if(getenv('HTTP_FORWARDED_FOR'))
        $ipaddress = getenv('HTTP_FORWARDED_FOR');
    else if(getenv('HTTP_FORWARDED'))
       $ipaddress = getenv('HTTP_FORWARDED');
    else if(getenv('REMOTE_ADDR'))
        $ipaddress = getenv('REMOTE_ADDR');
    else
        $ipaddress = 'UNKNOWN';
    return $ipaddress;
}

function generate_random_string($length, $keyspace = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ')
{
    //GENERATES RANDOM STRING ACCORDING TO PROVIDED LENGTH
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

function parse_email_template($raw_template, $replace)
{
    $pattern = '{{%s}}';
    $map = array();
    if($replace) {
        foreach($replace as $var => $value) {
            $map[sprintf($pattern, $var)] = $value;
        }
        $template = strtr($raw_template, $map);
    }
    return $template;
}
