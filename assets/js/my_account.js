$(document).ready(function() {
	//
	//FOR CHANGE PASSWORD STARTS HERE
	//Hide Change Password Section At First
	$('#change_password_section').hide();

	//Hide Submit Button at first
	$('#submit_btn_div').hide();

	//Checking Valid Password Or Not Starts Here
	$('#btn_check_password').click(function(){
	    var ajaxUrl = $('#baseUrl').val()+'my_account/checkValidPassword';
	    var current_password = $("#current_password").val();
	    if(current_password != "")
	    {
	    	$.ajax({
		        url: ajaxUrl,
		        data: {
		            current_password:current_password
		        },
		        type: 'post',
		        success: function (data) {
		            data = JSON.parse(data);   
		            // console.log(JSON.stringify(data));
		            // console.log(data.message);
		            if(data.status == true)
		            {
		            	$('#change_password_section').show();
		            	$('#current_password_section').hide();
		            }
		            else
		            {
		            	$('#lbl_current_password').text(data.message);
		            }
		        }//Success ends here
		    }); //Ajax ends here
	    }
	    else
	    {
	    	$('#lbl_current_password').text("Current Password Cannot Be Empty.");
	    }
	});//Change ends here
	//Checking Valid Password Or Not Ends Here

	//Checking Valid Password Or Not Starts Here
	$('#verify_new_password').keyup(function(){
		var new_password = $('#new_password').val();
		var verify_new_password = $('#verify_new_password').val();
		if(new_password == '' && verify_new_password=='')
		{
			$('#submit_btn_div').hide();
		}
		else if(new_password.length < 6)
		{
			$('#lbl_verify_new_password').text("New password must be at least of 6 characters.");
		}
		else if(new_password == verify_new_password)
		{
			$('#submit_btn_div').show();
			$('#lbl_verify_new_password').text("");
		}
		else
		{
			$('#submit_btn_div').hide();
			$('#lbl_verify_new_password').text("New Password and Verification Of New Password Mismatch.");
		}
	});
	//Checking Valid Password Or Not ends Here
	//FOR CHANGE PASSWORD ENDS HERE
	//
	//FOR CHANGE EMAIL STARTS HERE
	$('#main_change_email_div').hide();

	//FOR CHANGE EMAIL ENDS HERE
	$('#btn_change_password').click(function(){
		$('#main_change_password_div').show();
		$('#main_change_email_div').hide();
	});

	$('#btn_change_email').click(function(){
		$('#main_change_password_div').hide();
		$('#main_change_email_div').show();
	});
});

//Overriding Error Messages Starts
jQuery.extend(jQuery.validator.messages, {
    required: "This field is *required. <br/>",
    remote: "Please fix this field.",
    email: "Please enter a valid email address.",
    url: "Please enter a valid URL.",
    date: "Please enter a valid date.",
    dateISO: "Please enter a valid date (ISO).",
    number: "Please enter a valid number.",
    digits: "Please enter only digits.",
    creditcard: "Please enter a valid credit card number.",
    equalTo: "Please enter the same value again.",
    accept: "Please enter a value with a valid extension.",
    maxlength: jQuery.validator.format("Please enter no more than {0} characters."),
    minlength: jQuery.validator.format("Please enter at least {0} characters."),
    rangelength: jQuery.validator.format("Please enter a value between {0} and {1} characters long."),
    range: jQuery.validator.format("Please enter a value between {0} and {1}."),
    max: jQuery.validator.format("Please enter a value less than or equal to {0}."),
    min: jQuery.validator.format("Please enter a value greater than or equal to {0}.")
});
//Overriding error message ends
var validator = $("#change-email-formsubmit").validate({

    rules: {

        new_email_address: {
                required: true,
                email: true
            },

    },
    errorElement: 'span',
    errorClass: 'form-error',
    messages: {
        required: "* Required",
    },
    submitHandler: function (form) {
           $('#submit').attr('disabled','disabled');
           form.submit();
        }
});