
$(document).ready(function() {
	/*
	$('#dob').datetimepicker({
	        widgetPositioning: {
	          horizontal: 'left'
	        },
	        icons: {
	          time: "fa fa-clock-o",
	          date: "fa fa-calendar",
	          up: "fa fa-arrow-up",
	          down: "fa fa-arrow-down",
	          previous: 'fa fa-arrow-left',
	          next: 'fa fa-arrow-right'
	        },
	        format: 'LL'
	});

	$('.summernote').summernote({
	        height: 350
	});

	$("#frontdesk_formsubmit").submit(function(){
		var textareaValue = $('#summernote_remarks').summernote('code');
		$('#remarks').val(textareaValue);
	});
	*/
});
//Overriding Error Messages Starts
    jQuery.extend(jQuery.validator.messages, {
	    required: "This field is *required. <br/>",
	    remote: "Please fix this field.",
	    email: "Please enter a valid email address.",
	    url: "Please enter a valid URL.",
	    date: "Please enter a valid date.",
	    dateISO: "Please enter a valid date (ISO).",
	    number: "Please enter a valid number.",
	    digits: "Please enter only digits.",
	    creditcard: "Please enter a valid credit card number.",
	    equalTo: "Please enter the same value again.",
	    accept: "Please enter a value with a valid extension.",
	    maxlength: jQuery.validator.format("Please enter no more than {0} characters."),
	    minlength: jQuery.validator.format("Please enter at least {0} characters."),
	    rangelength: jQuery.validator.format("Please enter a value between {0} and {1} characters long."),
	    range: jQuery.validator.format("Please enter a value between {0} and {1}."),
	    max: jQuery.validator.format("Please enter a value less than or equal to {0}."),
	    min: jQuery.validator.format("Please enter a value greater than or equal to {0}.")
	});
	 //Overriding error message ends
    var validator = $("#labs_formsubmit").validate({

		rules: {
			title: "required"
	
		},
		errorElement: 'span',
        errorClass: 'form-error',
        messages: {
            required: "* Required",
        },
        submitHandler: function (form) {
		   $('#submit').attr('disabled','disabled');
		   $("#loading").show();
		   form.submit();
		}
	});